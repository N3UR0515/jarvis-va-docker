from functions.online_ops import find_my_ip, get_latest_news, get_random_advice, get_random_joke, get_trending_movies, get_weather_report, search_on_wikipedia
#from functions import retrieval
import speech_recognition as sr
import pyttsx3
import openai
import os
import streamlit as st
from pprint import pprint
import requests
import autogen
from dotenv import load_dotenv
load_dotenv()

config_list = autogen.config_list_from_json("OAI_CONFIG_LIST")
OPENAI_KEY = os.getenv('OPENAI_KEY')

import openai
openai.api_key = OPENAI_KEY

# Function to convert
# text to speech
def speak(command):

    try:
        # Initialise the engine
        engine = pyttsx3.init()
        # Change speech rate
        engine.setProperty('rate', 180)

        # Get available voices
        voices = engine.getProperty('voices')

        # choose a voice based on voice ID
        engine.setProperty('voice', voices[1].id)
        engine.say(command)
        engine.runAndWait()
        print(command)
    except:
        print("Error: Could not speak the text.")

# Initialise the recogniser
r = sr.Recognizer()

# Define a function to recognize speech
def recognize_speech():
    try:
        r = sr.Recognizer()
        with sr.Microphone() as source:
            audio = r.listen(source)
        query = r.recognize_google(audio)
        return query
    except KeyboardInterrupt:
        print("Keyboard interrupt detected. Closing the program...")
    except:
        print("Error: Could not recognize speech.")
        return ""

# Define a function to handle non-audible sounds
def handle_non_audible_sounds():
    try:
        r = sr.Recognizer()
        with sr.Microphone() as source:
            audio = r.listen(source)
        pass
        return True
    except:
        print("Error: Could not handle non-audible sounds.")
        pass
        return False
    

def send_to_chatGPT(messages, model="gpt-3.5-turbo"):
    # Send query to ChatGPT
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        max_tokens=100,
        n=1,
        stop=None,
        temperature=0.5,
    )

    message = response.choices[0].message.content
    messages.append(response.choices[0].message)
    return message

messages = [{"role": "user", "content": "Please, Act like Jarvis AI from Iron Man, without saying something to introduce yourself that sounds like this chat has already started. Be funny and a little sarcastic"}]

# create an AssistantAgent named "assistant"
assistant = autogen.AssistantAgent(
    name="assistant",
    llm_config={
        "seed": 42,  # seed for caching and reproducibility
        "config_list": config_list,  # a list of OpenAI API configurations
        "temperature": 0,  # temperature for sampling
    },  # configuration for autogen's enhanced inference API which is compatible with OpenAI API
)
# create a UserProxyAgent instance named "user_proxy"
user_proxy = autogen.UserProxyAgent(
    name="user_proxy",
    human_input_mode="NEVER",
    max_consecutive_auto_reply=10,
    is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
    code_execution_config={
        "work_dir": "../coding_output",
        "use_docker": False,  # set to True or image name like "python:3" to use docker
    },
)

# Main loop
def main():
    try:
        st.title("J.A.R.V.I.S")
        st.write("Press the 'Record' button to record your speech.")
        if st.button(label="Record", key='Record'):
            print("Listening...")
            #if st.button('Record'):
            message = st.empty()
            response = st.empty()
            message.text("Recording...")
            query = recognize_speech()
            message.text(f"User: {query}")
            #print("How can I help you?")
            while True:
                # Wait for the user's command
                query = recognize_speech()
                if "exit" in query.lower() or "stop" in query.lower():
                    speak("Goodbye!")
                    response.text(f"Jarvis: Goodbye!")
                    break
                else:
                    # Handle the user's command
                    if 'ip address' in query.lower():
                        ip_address = find_my_ip()
                        speak(f'Your IP Address is {ip_address}.\n For your convenience, I am printing it on the screen sir.')
                        response.text(f"Jarvis: Your IP Address is {ip_address}.\n For your convenience, I am printing it on the screen sir.")
                        print(f'Your IP Address is {ip_address}')
                        response.text(f"Jarvis: Your IP Address is {ip_address}")
                        break

                    elif 'wikipedia' in query.lower():
                        speak('What do you want to search on Wikipedia, sir?')
                        response.text(f"Jarvis: What do you want to search on Wikipedia Sir?")
                        search_query = recognize_speech()
                        results = search_on_wikipedia(search_query)
                        speak(f"According to Wikipedia, {results}")
                        response.text(f"Jarvis: According to Wikipedia:")
                        response.text(f"{results}")
                        speak("For your convenience, I am printing it on the screen sir.")
                        print(results)
                        break

                    elif 'joke' in query.lower():
                        speak(f"Hope you like this one Sir")
                        joke = get_random_joke()
                        speak(joke)
                        speak("For your convenience, I am also printing it on the screen Sir.")
                        pprint(joke)
                        response.text(f"{joke}")
                        break

                    elif "advice" in query.lower():
                        speak(f"Here's some advice for you, sir")
                        advice = get_random_advice()
                        speak(advice)
                        speak("For your convenience, I am also printing it on the screen Sir.")
                        pprint(advice)
                        response.text(f"Jarvis: Here is your advice:")
                        response.text(f"{advice}")
                        break

                    # elif "question" in query.lower():
                    #     speak(f"Ask away Sir!")
                    #     response.text(f"Jarvis: Ask away Sir!")
                    #     question = recognize_speech()
                    #     result = retrieval.agent_executor({"input": question})
                    #     speak(result["output"])
                    #     speak("For your convenience, I am printing it on the screen Sir.")
                    #     pprint(result["output"])
                    #     response.text(result["output"])
                    #     break

                    elif "trending movies" in query.lower():
                        speak(f"Some of the trending movies are: {get_trending_movies()}")
                        speak("For your convenience, I am printing it on the screen Sir.")
                        trending_movies = print(*get_trending_movies(), sep='\n')
                        response.text(f"Jarvis: Here are some trending movies:")
                        response.text(f"{trending_movies}")
                        break

                    elif 'news' in query.lower():
                        speak(f"I'm reading out the latest news headlines Sir")
                        speak(get_latest_news())
                        speak("For your convenience, I am printing it on the screen Sir.")
                        latest_news = print(*get_latest_news(), sep='\n')
                        response.text(f"Jarvis: Here is the latest news Sir:")
                        response.text(f"{latest_news}")
                        break

                    elif 'weather' in query.lower():
                        ip_address = find_my_ip()
                        city = requests.get(f"https://ipapi.co/{ip_address}/city/").text
                        speak(f"Getting weather report for your city {city}")
                        weather, temperature, feels_like = get_weather_report(city)
                        speak(f"The current temperature is {temperature}, but it feels like {feels_like}")
                        speak(f"Also, the weather report talks about {weather}")
                        speak("For your convenience, I am printing it on the screen Sir.")
                        print(f"Description: {weather}\nTemperature: {temperature}\nFeels like: {feels_like}")
                        response.text(f"Jarvis: Here is the weather Sir:")
                        response.text(f"Description: {weather}\nTemperature: {temperature}\nFeels like: {feels_like}")
                        break

                    elif 'python app' in query.lower():
                        speak('What do you want to create in Python Sir?')
                        query = recognize_speech()
                        speak('Requesting an agent create this for you.')
                        response.text('Requesting an agent create this for you.')
                        speak('Please see the terminal for code output.')
                        # the assistant receives a message from the user_proxy, which contains the task description
                        request_app = user_proxy.initiate_chat(
                            assistant,
                            message=query,
                        )
                        response.text({request_app})
                        is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE")
                        if is_termination_msg:
                            speak('Your Program has been created for you Sir.')
                            response.text("Your Program has been created for you Sir.")
                            speak('Please check the coding output folder for your code')
                            break
                        else:
                            speak('An agent is processing your request, Sir')
                            break

                    else:
                        messages.append({"role": "user", "content": query})
                        chatGPT = send_to_chatGPT(messages)
                        speak(chatGPT)
                        print(chatGPT)
                        response.text({chatGPT})
                        break
        else:
            handle_non_audible_sounds()
    except KeyboardInterrupt:
        print("Keyboard interrupt detected. Closing the program...")

if __name__ == "__main__":
    main()